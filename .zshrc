
export ZSH=/usr/share/oh-my-zsh
ZSH_THEME="powerlevel10k/powerlevel10k"
[[ $IN_IDEA != 1 ]] || ZSH_THEME="ys"
		

# To customize prompt, run `p10k configure` or edit ~/.p10k.zsh.
if [[ $ZSH_THEME == "powerlevel10k/powerlevel10k" ]]
then
	if [[ -r "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh" ]]; 
	then
  	source "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh"
	fi
	# echo "using p10k now"
	[[ ! -f ~/.p10k.zsh ]] || source ~/.p10k.zsh
	POWERLEVEL9K_CUSTOM_OS_ICON='echo   $(whoami) '
	POWERLEVEL9K_CUSTOM_OS_ICON_BACKGROUND=red
	POWERLEVEL9K_CUSTOM_OS_ICON_FOREGROUND=white
fi



DISABLE_AUTO_UPDATE="true"
ENABLE_CORRECTION="true"
COMPLETION_WAITING_DOTS="true"
# see 'man strftime' for details.
# HIST_STAMPS="mm/dd/yyyy"
plugins=(git web-search command-not-found colorize pip extract)

source ~/.zsh/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
source ~/.zsh/zsh-autosuggestions/zsh-autosuggestions.zsh

# User configuration
# export LANG=en_US.UTF-8
export VISUAL=vim
export EDITOR=$VISUAL

# color man page
export LESS_TERMCAP_mb=$'\e[1;32m'
export LESS_TERMCAP_md=$'\e[1;32m'
export LESS_TERMCAP_me=$'\e[0m'
export LESS_TERMCAP_se=$'\e[0m'
export LESS_TERMCAP_so=$'\e[01;33m'
export LESS_TERMCAP_ue=$'\e[0m'
export LESS_TERMCAP_us=$'\e[1;4;31m'

# key bindings
# bindkey '^[[2~' overwrite-mode
# bindkey '^[[3~' delete-char
# bindkey '^[[H' beginning-of-line
# bindkey '^[[1~' beginning-of-line
# bindkey '^[[F' end-of-line
# bindkey '^[[4~' end-of-line
# bindkey '^[[1;5C' forward-word
# bindkey '^[[1;5D' backward-word
# bindkey '^[[3;5~' kill-word


ZSH_CACHE_DIR=$HOME/.oh-my-zsh-cache
if [[ ! -d $ZSH_CACHE_DIR ]]; then
  mkdir $ZSH_CACHE_DIR
fi

source $ZSH/oh-my-zsh.sh

export PATH=${PATH}:/home/yujie6/bin:/home/yujie6/.gem/ruby/2.7.0/bin:/home/yujie6/.cargo/bin:/opt/Telegram:/home/yujie6/.local/bin/
export PATH=${PATH}:/opt/bin
export PATH=${PATH}:/home/yujie6/Documents/OS/Pintos/pintos/src/utils
# alias
alias zshconfig="vim ~/.zshrc"
alias ohmyzsh="vim ~/.oh-my-zsh"
alias ls='ls --color'
alias cls='colorls -l'
alias grep='grep --color=auto'
alias fgrep='fgrep --color=auto'
alias egrep='egrep --color=auto'
alias diff='diff --color=auto'
alias top='htop'
alias mx='cd ~/Documents/Compiler/MX-Compiler/src/main/java'

# fcitx env 
export GTK_IM_MODULE=fcitx
export QT_IM_MODULE=fcitx
export XMODIFIERS="@im=fcitx"
export XDG_CONFIG_HOME="$HOME/.config"
