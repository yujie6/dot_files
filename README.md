# My dotiles on Archlinx
## Applications
* window manager: i3-gaps
* terminal: alacritty
* shell: zsh
* status-bar: polyabr
* gtk-theme: sweet
* pdf-viewer: zathura
* image viewer: feh
* font: fira mono nerd font
* application launcher: rofi
* text editor: vim 

## Screen Shot


![Screen shot](./polybar/desktop.png)
